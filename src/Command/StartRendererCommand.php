<?php declare(strict_types=1);

namespace ShadowConnect\Command;

use ShadowConnect\Executor\RendererExecutor;
use ShadowConnect\Facade\Service;

final class StartRendererCommand extends AbstractCommand
{
    /** @var string */
    protected static $defaultName = 'start_renderer';

    public function execute(): int
    {
        $rendererExecutor = Service::get(RendererExecutor::class);
        $rendererExecutor->start();

        return 0;
    }
}
