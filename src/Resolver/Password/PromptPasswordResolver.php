<?php declare(strict_types=1);

namespace ShadowConnect\Resolver\Password;

use ShadowConnect\Resolver\AbstractResolver;

/**
 * Class PromptPasswordResolver
 *
 * @package    ShadowConnect\Resolver\Password
 * @author     Nicolas Guilloux <nguilloux@richcongress.com>
 * @copyright  2014 - 2020 RichCongress (https://www.richcongress.com)
 */
final class PromptPasswordResolver extends AbstractResolver
{
    /** @var string */
    protected static $propertyName = 'PASSWORD';

    /** @var int */
    protected static $priority = -100;

    public function resolve(): string
    {
        return $this->promptPassword('Password: ');
    }

    /**
     * @ref https://stackoverflow.com/questions/187736/command-line-password-prompt-in-php
     */
    private function promptPassword(string $prompt): ?string
    {
        if (stripos(PHP_OS, 'win') === 0) {
            $vbscript = sys_get_temp_dir() . 'prompt_password.vbs';
            file_put_contents(
                $vbscript, 'wscript.echo(InputBox("'
                . addslashes($prompt)
                . '", "", "password here"))');
            $command = "cscript //nologo " . escapeshellarg($vbscript);
            $password = rtrim(shell_exec($command));
            unlink($vbscript);

            return $password;
        }

        $command = "/usr/bin/env bash -c 'echo OK'";

        if (rtrim(shell_exec($command)) !== 'OK') {
            trigger_error("Can't invoke bash");
            return null;
        }

        $command = "/usr/bin/env bash -c 'read -s -p \"" . addslashes($prompt) . "\" mypassword && echo \$mypassword'";
        $password = rtrim(shell_exec($command));
        echo "\n";

        return $password;
    }
}
