<?php declare(strict_types=1);

namespace ShadowConnect\Resolver\Platform;

use ShadowConnect\Resolver\AbstractResolver;

/**
 * Class PlaftormResolver
 *
 * @package    ShadowConnect\Resolver\Platform
 * @author     Nicolas Guilloux <nguilloux@richcongress.com>
 * @copyright  2014 - 2020 RichCongress (https://www.richcongress.com)
 */
final class PlaftormResolver extends AbstractResolver
{
    /** @var string */
    protected static $propertyName = 'PLATFORM';

    public function resolve(): string
    {
        return PHP_OS;
    }
}
