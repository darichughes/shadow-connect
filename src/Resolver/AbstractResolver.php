<?php declare(strict_types=1);

namespace ShadowConnect\Resolver;

/**
 * Class AbstractResolver
 *
 * @package    ShadowConnect\Resolver
 * @author     Nicolas Guilloux <nguilloux@richcongress.com>
 * @copyright  2014 - 2020 RichCongress (https://www.richcongress.com)
 */
abstract class AbstractResolver implements ResolverInterface
{
    /** @var string|null */
    protected static $propertyName;

    /** @var int */
    protected static $priority = 0;

    public function supports(string $key): bool
    {
        return $key === static::$propertyName;
    }

    public function getPriority(): int
    {
        return static::$priority;
    }
}
