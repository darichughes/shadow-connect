<?php declare(strict_types=1);

namespace ShadowConnect\Handler;

use ShadowConnect\Facade\Logger;

final class VmProxySocketResponseHandler
{
    /**
     * @param array<string, string> $content
     */
    public function handle(array $content): bool
    {
        switch ($content['cmd'] ?? null) {
            case 'get-version':
                Logger::info('Got VMProxy version: ' . $content['version'] ?? null);
                return true;

            case 'ping':
                Logger::debug('Got Ping! Pong id: ' . $content['id'] ?? null);
                return true;

            case 'bsod':
                throw new \LogicException('BSOD! ' . $content['error'] ?? null);

            case 'getout':
                throw new \LogicException('Get out received. Ciao!');

            default:
                Logger::debug('Unknown command: ' . $content['cmd'] ?? null);
                return true;
        }
    }
}
