<?php declare(strict_types=1);

namespace ShadowConnect\Handler;

use ShadowConnect\Api\GapApi;
use ShadowConnect\Api\Utils\JsonResponse;
use ShadowConnect\Facade\Configuration;
use ShadowConnect\Facade\Logger;
use ShadowConnect\Facade\Service;
use ShadowConnect\Helper\Output;

final class ApiErrorHandler
{
    private const ERROR_MAPPING = [
        'Invalid session (3)'                  => 'checkUuid',
        'Unable to find the associated client' => 'registerUuid',
    ];

    /** @var GapApi */
    private $gapApi;

    public function __construct()
    {
        /** @var GapApi $gapApi */
        $gapApi = Service::get(GapApi::class);
        $this->gapApi = $gapApi;
    }

    public function supports(JsonResponse $response): bool
    {
        return array_key_exists(
            $response->getContent()['err'] ?? 0,
            self::ERROR_MAPPING
        );
    }

    public function handle(JsonResponse $response): bool
    {
        if (!$this->supports($response)) {
            return false;
        }

        $error = $response->getContent()['err'] ?? '';
        $method = self::ERROR_MAPPING[$error];
        /** @var callable $callback */
        $callback = [$this, $method];

        return $callback($response);
    }

    private function checkUuid(): bool
    {
        $this->gapApi->checkUuid();

        return false;
    }

    private function registerUuid(): bool
    {
        $code = '';
        $promptMessage = sprintf(
            'Please enter the code you received on your mailbox %s and press enter: ',
            Configuration::get('USERNAME')
        );

        while (!preg_match('/^[A-Za-z0-9_-]{6}$/', $code)) {
            $code = Output::prompt($promptMessage);
        }

        Logger::debug('Using user code: ' . $code);
        $response = $this->gapApi->registerUuid($code);

        if ($response->isFailed()) {
            Output::error('The code was not valid. Please retry.');
            $this->registerUuid();
        }

        return true;
    }
}
