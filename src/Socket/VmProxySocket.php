<?php declare(strict_types=1);

namespace ShadowConnect\Socket;

use ShadowConnect\Facade\Configuration;
use ShadowConnect\Facade\Service;
use ShadowConnect\Handler\VmProxySocketResponseHandler;
use ShadowConnect\Socket\Utils\AbstractSocket;

final class VmProxySocket extends AbstractSocket
{
    /** @var int */
    protected static $portOffset = 7001;

    /**
     * @return resource|null
     */
    protected function getSocketClient()
    {
        $socket = parent::getSocketClient();

        $this->send([
            'cmd' => 'get-version',
            'id'  => 1,
        ]);

        $this->send([
            'cmd' => 'hello',
            'id'  => 2,
        ]);

        sleep(4);

        return $socket;
    }

    protected function checkVmComponents(): void
    {
        $this->send([
            'cmd'     => 'forward',
            'service' => 'shadow-status',
            'data'    => [
                'type'  => 'is_up_to_date',
                'value' => Configuration::get('BRANCH'),
            ],
        ]);
    }

    protected function handle(string $message): bool
    {
        /** @var VmProxySocketResponseHandler $handler */
        $handler = Service::get(VmProxySocketResponseHandler::class);
        $content = json_decode($message, true);

        return $handler->handle($content);
    }
}
