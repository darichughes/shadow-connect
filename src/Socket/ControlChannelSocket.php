<?php declare(strict_types=1);

namespace ShadowConnect\Socket;

use ShadowConnect\Socket\Utils\AbstractSocket;

final class ControlChannelSocket extends AbstractSocket
{
    /** @var int */
    protected static $portOffset = 7011;

    public function isAlive(): bool
    {
        if ($this->ip === null) {
            $this->parseVmInfo();
        }

        $fp = fsockopen($this->ip, $this->port, $errno, $errstr, 0.1);

        if ($fp !== false) {
            fclose($fp);
            return true;
        }

        return false;
    }
}
