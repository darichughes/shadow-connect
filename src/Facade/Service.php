<?php declare(strict_types=1);

namespace ShadowConnect\Facade;

final class Service
{
    /** @var array|object[] */
    private static $container = [];

    /**
     * @return object
     */
    public static function get(string $class)
    {
        if (!array_key_exists($class, self::$container)) {
            self::$container[$class] = new $class();
        }

        return self::$container[$class];
    }
}
