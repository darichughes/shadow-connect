<?php declare(strict_types=1);

namespace ShadowConnect;

use ShadowConnect\Command\AbstractCommand;
use ShadowConnect\Facade\Configuration;
use ShadowConnect\Facade\Service;
use ShadowConnect\Helper\Output;

/**
 * Class Kernel
 *
 * @package    ShadowConnect
 * @author     Nicolas Guilloux <nguilloux@richcongress.com>
 * @copyright  2014 - 2020 RichCongress (https://www.richcongress.com)
 */
final class Kernel
{
    private const COMMAND_NAMESPACE = 'ShadowConnect\\Command';

    public function __construct()
    {
        Configuration::init();
    }

    public function process(?string $command): int
    {
        $commands = $this->getCommands();
        $commandClass = $commands[$command] ?? null;

        if ($commandClass === null) {
            $commandNames = array_keys($commands);
            $error = sprintf('Usage: shadow-connect (%s)', implode('|', $commandNames));
            throw new \LogicException($error);
        }

        return Service::get($commandClass)->execute();
    }

    public static function getProjectDir(): string
    {
        return __DIR__ . '/..';
    }

    /**
     * Get all the commands by scanning the command folder
     *
     * @return string[]
     */
    private function getCommands(): array
    {
        $commandsDir = self::getProjectDir() . '/src/Command';
        $commands = [];

        foreach (scandir($commandsDir) as $file) {
            if (in_array($file, ['.', '..', 'AbstractCommand.php'], true)) {
                continue;
            }

            /** @var string|AbstractCommand $class */
            $class = self::COMMAND_NAMESPACE . '\\' . str_replace('.php', '', $file);
            $commands[$class::getDefaultName()] = $class;
        }

        return $commands;
    }
}
