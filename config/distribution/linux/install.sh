#!/usr/bin/env bash

TARGET_DIR="$HOME/.local/share"
BIN_DIR="$HOME/.local/bin"

# Create target directory
mkdir -p $TARGET_DIR
cd $TARGET_DIR

# Clone git
echo "Cloning the repository in $PWD"
git clone git@gitlab.com:NicolasGuilloux/shadow-connect.git
cd shadow-connect

# Register the binary
ln -s "./bin/shadow-connect" "$BIN_DIR/shadow-connect"

# Execute the configuration wizard
./bin/shadow-connect config-wizard


