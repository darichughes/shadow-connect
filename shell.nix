with import <nixpkgs> { };

let
  patchedPhp = stdenv.mkDerivation rec {
    name = "shadow-connect";
    buildInputs = with pkgs; [ php php74Extensions.xdebug makeWrapper ];

    env = buildEnv {
      inherit buildInputs name;
      paths = buildInputs;
      postBuild = ''
        mkdir $out/bin.writable && cp --symbolic-link `readlink $out/bin`/* $out/bin.writable/ && rm $out/bin && mv $out/bin.writable $out/bin
        wrapProgram $out/bin/php --add-flags "-d extension_dir=$out/lib/php/extensions -d zend_extension=xdebug.so"
      '';
    };

    builder = builtins.toFile "builder.sh" ''
      source $stdenv/setup; ln -s $env $out
    '';
  };
in pkgs.mkShell { buildInputs = [ patchedPhp pkgs.php74Packages.composer ]; }
